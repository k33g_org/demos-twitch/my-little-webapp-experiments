package main

import (
	"fmt"
	"testing"
)

// GOOS=js GOARCH=wasm go test -exec="$(go env GOROOT)/misc/wasm/go_js_wasm_exec"

func TestSayHello(t *testing.T) {
	
	result := SagHallo()
	fmt.Println(result)

	expected_result := "hallo"

	if result != expected_result {
		t.Error("Incorrect result, expected '"+ expected_result +"', got", result)
	}
}
