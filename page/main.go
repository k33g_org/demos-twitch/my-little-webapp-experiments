package main

import (
	"fmt"
	"syscall/js"

)

var ciProjectId string
var ciMergeRequestIid string
var ciProjectPath string
var ciCommitRefSlug string

var ffApiUrl string
var ffInstanceId string
var ffProductionEnvironment string

type metricsInterface struct {
}

func Html(_ js.Value, args []js.Value) interface{} {
	page := fmt.Sprintf(`
	<h1>🌕 Hello World 🚀</h1>
	<h2>GoLang is amazing</h2>
	<h3>Wasm is fantastic</h3>
	<ul>
		<li>%v</li>
		<li>%v</li>
		<li>%v</li>
	</ul>
	`,ffApiUrl, ffInstanceId, ffProductionEnvironment)
	return page
}



func main() {

	go func() {
		js.Global().Call("startCb")
	}()

	js.Global().Set("Html", js.FuncOf(Html))

	<-make(chan bool)
}
