#!/bin/bash
export CI_PROJECT_ID="00000"
export CI_MERGE_REQUEST_IID="11111"
export CI_PROJECT_PATH="SOMEWHERE"
export CI_COMMIT_REF_SLUG="main"
export API_URL="http://somewhere"
export INSTANCE_ID="333"
export ENVIRONMENT="prod"

#tinygo build \
#  -ldflags "-X main.ciProjectId=$CI_PROJECT_ID -X main.ciMergeRequestIid=$CI_MERGE_REQUEST_IID -X main.ciProjectPath=$CI_PROJECT_PATH -X main.ciCommitRefSlug=$CI_COMMIT_REF_SLUG"" \
#  -o index.wasm -target wasm ./main.go



#tinygo build \
#  -ldflags "-X main.ciProjectId=$CI_PROJECT_ID -X main.ciMergeRequestIid=$CI_MERGE_REQUEST_IID -X main.ciProjectPath=$CI_PROJECT_PATH -X main.ciCommitRefSlug=$CI_COMMIT_REF_SLUG -X main.ffApiUrl=$API_URL -X main.ffInstanceId=$INSTANCE_ID -X main.ffProductionEnvironment=$ENVIRONMENT" \
#  -o index.wasm -target wasm ./main.go

GOOS=js GOARCH=wasm go build \
  -ldflags "-X main.ciProjectId=$CI_PROJECT_ID -X main.ciMergeRequestIid=$CI_MERGE_REQUEST_IID -X main.ciProjectPath=$CI_PROJECT_PATH -X main.ciCommitRefSlug=$CI_COMMIT_REF_SLUG -X main.ffApiUrl=$API_URL -X main.ffInstanceId=$INSTANCE_ID -X main.ffProductionEnvironment=$ENVIRONMENT" \
  -o go.index.wasm ./main.go

  ls -lh *.wasm
  